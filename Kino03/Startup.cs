﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kino03.Startup))]
namespace Kino03
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
