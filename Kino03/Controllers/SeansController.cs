﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kino03.Models;

namespace Kino03.Controllers
{
    
    public class SeansController : Controller
    {
        private DB_A25195_KinoEntities db = new DB_A25195_KinoEntities();

        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Index()
        {
            var seans = db.Seans.Include(s => s.Film).Include(s => s.Sala);
            return View(seans.ToList());
        }

        [AllowAnonymous]
        public ActionResult Views()
        {
            return View(db.Seans.ToList());
        }



        [Authorize]
        public ActionResult Wybierz()
        {
            return View(db.Seans.ToList());
        }






        // GET: Seans/Details/5
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seans seans = db.Seans.Find(id);
            if (seans == null)
            {
                return HttpNotFound();
            }
            return View(seans);
        }


        // GET: Seans/Create
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Create()
        {
            ViewBag.idFilm = new SelectList(db.Film, "idFilm", "Tytuł");
            ViewBag.idSala = new SelectList(db.Sala, "idSala", "Numer");
            return View();
        }

        // POST: Seans/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Create([Bind(Include = "idSeans,idSala,idFilm,Data,WersjaJęzykowa,Wymiar")] Seans seans)
        {
            if (ModelState.IsValid)
            {
                db.Seans.Add(seans);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idFilm = new SelectList(db.Film, "idFilm", "Tytuł", seans.idFilm);
            ViewBag.idSala = new SelectList(db.Sala, "idSala", "Numer", seans.idSala);
            return View(seans);
        }

        // GET: Seans/Edit/5
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seans seans = db.Seans.Find(id);
            if (seans == null)
            {
                return HttpNotFound();
            }
            ViewBag.idFilm = new SelectList(db.Film, "idFilm", "Tytuł", seans.idFilm);
            ViewBag.idSala = new SelectList(db.Sala, "idSala", "idSala", seans.idSala);
            return View(seans);
        }

        // POST: Seans/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Edit([Bind(Include = "idSeans,idSala,idFilm,Data,WersjaJęzykowa,Wymiar")] Seans seans)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seans).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idFilm = new SelectList(db.Film, "idFilm", "Tytuł", seans.idFilm);
            ViewBag.idSala = new SelectList(db.Sala, "idSala", "idSala", seans.idSala);
            return View(seans);
        }

        // GET: Seans/Delete/5
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seans seans = db.Seans.Find(id);
            if (seans == null)
            {
                return HttpNotFound();
            }
            return View(seans);
        }

        // POST: Seans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Seans seans = db.Seans.Find(id);
            db.Seans.Remove(seans);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
