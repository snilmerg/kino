﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kino03.Models;
using Microsoft.AspNet.Identity;

namespace Kino03.Controllers
{
    
    public class RezerwacjasController : Controller
    {


        private DB_A25195_KinoEntities db = new DB_A25195_KinoEntities();

        // GET: Rezerwacjas
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Index()
        {
            var rezerwacja = db.Rezerwacja.Include(r => r.Seans).Include(r => r.AspNetUsers);
            return View(rezerwacja.ToList());
        }


        [Authorize]
        public ActionResult MojeRezerwacje()
        {
            var dana = User.Identity.GetUserId();
            var zm = db.Rezerwacja.Where(d => d.idUser == dana);

            return View(zm.ToList());
        }





        // GET: Rezerwacjas/Create
        [Authorize]
        public ActionResult Zarezerwuj(int id)
        {
            // wybór seansu z odpowiednim id
            var zm = db.Seans.Where(d => d.idSeans == id);
            ViewBag.idSeans = new SelectList(zm, "idSeans", "idSeans");

            ViewBag.idData = new SelectList(db.Seans, "idSeans", "Data");

            // wybór aktualnego użytkownika
            var idee = User.Identity.GetUserId();
            var zm2 = db.AspNetUsers.Where(x => x.Id == idee);
            ViewBag.idUser = new SelectList(zm2, "Id", "Email");

            List<Int32> rzedy = new List<Int32>();
            List<Int32> kolumny = new List<Int32>();
            int z = db.Seans.Find(id).Sala.Rzędy;
            int y = db.Seans.Find(id).Sala.Kolumny;



            for(int i=1; i<z+1; i++)
            {
                rzedy.Add(i);
            }
            for (int i = 1; i < y + 1; i++)
            {
                kolumny.Add(i);
            }


            var zList = rzedy.Select((p, i) => new { Value = (p < 12 ? p : 12), Data = p }).ToList();
            ViewBag.rzad = new SelectList(zList, "Value", "Data");

            var yList = kolumny.Select((p, i) => new { Value = (p < 12 ? p : 12), Data = p }).ToList();
            ViewBag.kolumna = new SelectList(yList, "Value", "Data");


            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Zarezerwuj([Bind(Include = "idRezerwacja,idSeans,rzad,kolumna,idUser")] Rezerwacja rezerwacja)
        {
            if (ModelState.IsValid)
            {
                foreach(var tmp in db.Rezerwacja)
                {
                    if(tmp.idSeans == rezerwacja.idSeans && tmp.rzad == rezerwacja.rzad && tmp.kolumna == rezerwacja.kolumna)
                    {
                        {
                            db.SaveChanges();
                            return RedirectToAction("MojeRezerwacje", "Rezerwacjas");
                        }
                    }

                }


                db.Rezerwacja.Add(rezerwacja);
                db.SaveChanges();
                return RedirectToAction("MojeRezerwacje", "Rezerwacjas");
            }

            ViewBag.idSeans = new SelectList(db.Seans, "idSeans", "idSeans", rezerwacja.idSeans);
            ViewBag.idUser = new SelectList(db.AspNetUsers, "Id", "Email", rezerwacja.idUser);
            return View(rezerwacja);
        }



        // GET: Rezerwacjas/Details/5
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezerwacja rezerwacja = db.Rezerwacja.Find(id);
            if (rezerwacja == null)
            {
                return HttpNotFound();
            }
            return View(rezerwacja);
        }

        // GET: Rezerwacjas/Create
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Create()
        {
            ViewBag.idSeans = new SelectList(db.Seans, "idSeans", "idSeans");
            ViewBag.idData = new SelectList(db.Seans, "idSeans", "Data");
            ViewBag.idUser = new SelectList(db.AspNetUsers, "Id", "Email");
            return View();
        }

        // POST: Rezerwacjas/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Create([Bind(Include = "idRezerwacja,idSeans,rzad,kolumna,idUser")] Rezerwacja rezerwacja)
        {
            if (ModelState.IsValid)
            {
                db.Rezerwacja.Add(rezerwacja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idSeans = new SelectList(db.Seans, "idSeans", "idSeans", rezerwacja.idSeans);
            ViewBag.idUser = new SelectList(db.AspNetUsers, "Id", "Email", rezerwacja.idUser);
            return View(rezerwacja);
        }

        // GET: Rezerwacjas/Edit/5
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezerwacja rezerwacja = db.Rezerwacja.Find(id);
            if (rezerwacja == null)
            {
                return HttpNotFound();
            }
            ViewBag.idSeans = new SelectList(db.Seans, "idSeans", "WersjaJęzykowa", rezerwacja.idSeans);
            ViewBag.idUser = new SelectList(db.AspNetUsers, "Id", "Email", rezerwacja.idUser);
            return View(rezerwacja);
        }

        // POST: Rezerwacjas/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "admin@admin.admin")]
        public ActionResult Edit([Bind(Include = "idRezerwacja,idSeans,rzad,kolumna,idUser")] Rezerwacja rezerwacja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rezerwacja).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idSeans = new SelectList(db.Seans, "idSeans", "WersjaJęzykowa", rezerwacja.idSeans);
            ViewBag.idUser = new SelectList(db.AspNetUsers, "Id", "Email", rezerwacja.idUser);
            return View(rezerwacja);
        }

        // GET: Rezerwacjas/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezerwacja rezerwacja = db.Rezerwacja.Find(id);
            if (rezerwacja == null)
            {
                return HttpNotFound();
            }
            return View(rezerwacja);
        }

        // POST: Rezerwacjas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Rezerwacja rezerwacja = db.Rezerwacja.Find(id);
            db.Rezerwacja.Remove(rezerwacja);
            db.SaveChanges();
            return RedirectToAction("MojeRezerwacje", "Rezerwacjas");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
