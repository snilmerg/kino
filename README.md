# README #

* Repozytorium projektu bazy danych do rezerwacji biletów kinowych
* Projekt powstał wyłącznie do celów edukacyjnych
* Autorzy: Mariusz Wasilewski, **Maciej Piórek**

# SETUP #

* Projekt jest od razu gotowy do skompilowania i uruchomienia
* Baza danych umieszczona na zewnętrznym serwisie jest aktywna do 17.07.2017

# ADMIN #

* Panel admina dostępny jest po zalogowaniu na konto (mail /pw): admin@admin.admin / Maciej123_

# ADDITIONALY #

* Autorzy proszą o nie wykorzystywanie projektu do celów komercyjnych oraz nie rozpowszechnianie bez zgody autorów